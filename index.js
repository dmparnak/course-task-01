const express = require('express');
const app = express();

const PORT = process.env.PORT ?? 56201;

app.use(express.text());

app.post('/square',(req, res) => {
    const number = +req.body;
    const square = number * number;
    res.send({
        'number': number,
        'square': square
    });
})

app.post('/reverse', (req, res) => {
    const text = req.body;
    const reversedString = text.split('').reverse().join('');
    res.send(reversedString);
});

function isLeapYear(year) {
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}


app.get('/date/:year/:month/:day', (req, res) => {
    const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const year = parseInt(req.params.year);
    const month = parseInt(req.params.month);
    const day = parseInt(req.params.day);

    const inputDate = new Date(year, month - 1, day);
    const nowDate = new Date();

    const msPerDay = 1000 * 60 * 60 * 24;
    const difference = Math.abs(Math.ceil((inputDate - nowDate) / msPerDay));

    res.send({
        'weekDay': daysOfWeek[inputDate.getDay()],
        'isLeapYear': isLeapYear(year),
        'difference': difference
    });
});

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`);
});